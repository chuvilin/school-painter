Painter {
    width: 640
    height: 480

    function main() {
        fillStyle = "white"
        fillRect(0, 0, width, height)

        lineWidth = 2
    }

    onMouseEvent: {
        var mouseEvent = readMouse()
        if (mouseEvent.buttons) {
            switch (Math.round(Math.random() * 3)) {
            case 0:
                fillStyle = "red"
                strokeStyle = "green"
                break
            case 1:
                fillStyle = "green"
                strokeStyle = "blue"
                break
            case 2:
                fillStyle = "blue"
                strokeStyle = "red"
                break
            }
            fillRect(mouseEvent.x - 5, mouseEvent.y - 5, 10, 10)
            beginPath()
            moveTo(mouseEvent.x, mouseEvent.y)
        } else {
            lineTo(mouseEvent.x, mouseEvent.y)
            stroke()
            fillRect(mouseEvent.x - 5, mouseEvent.y - 5, 10, 10)
        }
    }

    onKeyEvent: {
        var keyEvent = readKey()
        switch (keyEvent.key) {
        case Qt.Key_Escape:
            exit(0)
            break
        default:
            console.log("Unknown key", keyEvent.key)
        }
    }
}
