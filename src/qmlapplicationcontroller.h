#ifndef QMLAPPLICATIONCONTROLLER_H
#define QMLAPPLICATIONCONTROLLER_H

#include <QObject>

class QQmlApplicationEngine;

class QmlApplicationController : public QObject
{
    Q_OBJECT
public:
    explicit QmlApplicationController(QQmlApplicationEngine *engine, QObject *parent = nullptr);

public slots:
    void wait(uint ms = 1000) const;
    void processEvents() const;
    void exit(int retcode = 0);

private:
    QQmlApplicationEngine *m_engine;
};

#endif // QMLAPPLICATIONCONTROLLER_H
