#include "qmlapplicationcontroller.h"

#include <QCoreApplication>
#include <QQmlApplicationEngine>
#include <QTime>
#include <QTimer>

QmlApplicationController::QmlApplicationController(QQmlApplicationEngine *engine, QObject *parent) :
    QObject(parent),
    m_engine(engine)
{
}

void QmlApplicationController::wait(uint ms) const
{
    auto stopTime = QTime::currentTime().addMSecs(ms);
    while (QTime::currentTime() < stopTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, QTime::currentTime().msecsTo(stopTime));
}

void QmlApplicationController::processEvents() const
{
    QCoreApplication::processEvents(QEventLoop::AllEvents);
}

void QmlApplicationController::exit(int retcode)
{
    m_engine->setInterrupted(true);
    QTimer::singleShot(100, this, [retcode]() {
        QCoreApplication::exit(retcode);
    });
}
