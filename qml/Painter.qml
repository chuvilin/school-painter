import QtQuick 2.0
import QtQuick.Window 2.1

Window {
    id: root

    property var fillStyle: "#000000"
    property real lineWidth: 0
    property var strokeStyle: "#000000"

    function beginPath(){
        canvas.operations.push(function () { canvas.context.beginPath() } )
        canvas.requestPaint()
    }

    function exit(retcode = 0) { ApplicationController.exit(retcode) }

    function fillRect(x, y, width, height) {
        canvas.operations.push(function () { canvas.context.fillRect(x, y, width, height) } )
        canvas.requestPaint()
    }

    function lineTo(x, y) {
        canvas.operations.push(function () { canvas.context.lineTo(x, y) } )
        canvas.requestPaint()
    }

    function moveTo(x, y) {
        canvas.operations.push(function () { canvas.context.moveTo(x, y) } )
        canvas.requestPaint()
    }

    function processEvents() { ApplicationController.processEvents() }

    function readKey() {
        ApplicationController.processEvents()
        var event = eventArea.keyEvents.shift()
        return event
    }

    function readMouse() {
        ApplicationController.processEvents()
        var event = eventArea.mouseEvents.shift()
        return event
    }

    function stroke(){
        canvas.operations.push(function () { canvas.context.stroke() } )
        canvas.requestPaint()
    }

    function wait(ms) { ApplicationController.wait(ms) }

    function waitKey() {
        while (true) {
            ApplicationController.processEvents()
            var event = eventArea.keyEvents.shift()
            if (event)
                return event
        }
    }

    function waitMouse() {
        while (true) {
            ApplicationController.processEvents()
            var event = eventArea.mouseEvents.shift()
            if (event)
                return event
        }
    }

    signal keyEvent()
    signal mouseEvent()

    visible: true
    title: qsTr("QML Painter")

    onClosing: ApplicationController.exit(0)

    onFillStyleChanged: {
        var fillStyle = root.fillStyle
        canvas.operations.push(function () { canvas.context.fillStyle = fillStyle } )
        canvas.requestPaint()
    }

    onLineWidthChanged: {
        var lineWidth = root.lineWidth
        canvas.operations.push(function () { canvas.context.lineWidth = lineWidth } )
        canvas.requestPaint()
    }

    onStrokeStyleChanged: {
        var strokeStyle = root.strokeStyle
        canvas.operations.push(function () { canvas.context.strokeStyle = strokeStyle } )
        canvas.requestPaint()
    }

    Component.onCompleted: {
        if (root.main)
            root.main()
    }

    Canvas {
        id: canvas

        property var operations: []

        anchors.fill: parent
        renderStrategy: Canvas.Threaded

        onPaint: {
            getContext("2d")
            operations.forEach(function (operation) {
                operation()
            })
            operations = []
        }
    }

    MouseArea {
        id: eventArea

        property var keyEvents: []
        property var mouseEvents: []

        anchors.fill: parent
        focus: true

        onPressed: {
            mouseEvents.push(mouse)
            root.mouseEvent()
        }

        onReleased: {
            mouseEvents.push(mouse)
            root.mouseEvent()
        }

        Keys.onPressed: {
            keyEvents.push(event)
            root.keyEvent()
        }
    }
}
